import org.junit.Test;
import top.yangbuyi.utils.OSSUploadUtil;
import top.yangbuyi.utils.OSSUtils;

import java.io.File;

/**
 * @description: 杨不易网站:www.yangbuyi.top
 * @program: yangbuyispringcloudparent
 * @ClassName: two
 * @create: 2020-08-12 00:06
 * @author: yangbuyi
 * @since： JDK1.8
 * @two: OSSUploadUtil.uploadFile(File file, String fileType) //单文件上传，type:文件后缀名
 * OSSUploadUtil.updateFile(File file, String fileType, String oldUrl)//更新文件:只更新内容，不更新文件名和文件地址。
 * OSSUploadUtil.replaceFile(File file, String fileType, String oldUrl)//替换文件，删除源文件并上传新文件，文件名和地址也改变
 * OSSUploadUtil.deleteFile(List<String> fileUrls)  //删除多文件，根据问价url来自定获取其中的bucket和文件名，用于bucket和文件名可能存在不同的，循环调用deleteFile方法
 * OSSUploadUtil.deleteFile(String fileUrl) //删除单文件
 * OSSUploadUtil.deleteFiles(List<String> fileUrls)  //删除多文件，根据配置直接取删除多个文件，bucket和文件地址从配置中获取，用于多文件bucket和文件名都相同的
 **/

public class two {

	  @Test
	  public void t() {
			// 文件上传
			File file = new File("F:\\美女图片\\dac18dd66fbbf53ddd55563b5edecac6.jpg");
			String jpg = OSSUploadUtil.uploadFile(file, "jpg");
			System.out.println(jpg);


			// 更新 只更新 图片  不更新名称
			System.out.println("更新图片开始:" + "http://oss-img.yangbuyi.top/github.png");
			File file2 = new File("F:\\美女图片\\httpsimg2020.cnblogs.comblog17352552020081735255-20200805220326890-2058657956.jpg");
			String jpg1 = OSSUploadUtil.updateFile(file2, "jpg", "http://oss-img.yangbuyi.top/github.png");
			System.out.println("更新后的：" + jpg1);

			// 删除  根据url 删除图片
//			boolean b = OSSUploadUtil.deleteFile("https://yangbuyi.oss-cn-beijing.aliyuncs.com/imgBAA8AD5484D847E09BDB535765A9EEB9.jpg");
//			System.out.println(b);

	  }


}
